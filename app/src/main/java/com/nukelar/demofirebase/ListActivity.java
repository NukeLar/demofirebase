package com.nukelar.demofirebase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    ListView listView;
   // private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        Intent intent = getIntent();
        final int userId = intent.getIntExtra("id", 100);

        //fireBase
       // createNewUser();

        //createRecord();


        //reading
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("records");

        mDatabase.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot snapshot) {

                listView = (ListView) findViewById(R.id.list);

                ArrayList<String> records = new ArrayList<String>();

                for (DataSnapshot postSnapshot: snapshot.getChildren()) {
                    Record record = postSnapshot.getValue(Record.class);
                    if (record.getId() == userId)
                        records.add(record.getRecord());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                        android.R.layout.simple_list_item_1, android.R.id.text1, records);

                listView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError error) {
            }
        });

//        mDatabase.child("-Kk0sOpGqnU9qyE8IrBj").addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                User user = dataSnapshot.getValue(User.class);
//                listView = (ListView) findViewById(R.id.list);
//
//                //test data
//                String[] values = new String[] {user.getUserName(),
//                        user.getPassword()
//                };
//
//                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
//                        android.R.layout.simple_list_item_1, android.R.id.text1, values);
//
//                listView.setAdapter(adapter);
//            }
//
//            @Override
//            public void onCancelled(DatabaseError error) {
//                // Failed to read value
//            }
//        });


    }

    private void createRecord() {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("records");

        // Creating new user node, which returns the unique key value
        // new user node would be /users/$userid/
        String recordId = mDatabase.push().getKey();

        // creating user object
        Record record = new Record(0, "233");

        // pushing user to 'users' node using the userId
        mDatabase.child(recordId).setValue(record);
    }

    private void createNewUser() {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("users");

        // Creating new user node, which returns the unique key value
        // new user node would be /users/$userid/
        String userId = mDatabase.push().getKey();

        // creating user object
        User user = new User("admin", "admin", 1);

        // pushing user to 'users' node using the userId
        mDatabase.child(userId).setValue(user);
    }
}