package com.nukelar.demofirebase;

/**
 * Created by NukeLar on 05/12/17.
 */

public class Record {

    public int id;
    public String record;

    public Record() {
    }

    public Record(int id, String record) {
        this.id = id;
        this.record = record;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRecord() {
        return record;
    }

    public void setRecord(String record) {
        this.record = record;
    }
}
